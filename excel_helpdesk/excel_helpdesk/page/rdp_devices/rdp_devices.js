/**
 * Manages the inventory of devices, including UI interactions and API calls,
 * for a Frappe application.
 */
class DeviceInventory {
    /**
     * Initializes the device inventory page.
     * @param {Object} wrapper - The wrapper element for the device inventory page.
     */
    constructor(wrapper) {
        this.page = frappe.ui.make_app_page({
            parent: wrapper,
            title: 'Device Inventory',
            single_column: true
        });
        this.rdpData=[];
        this.deviceData = [];
        this.fullNamelist=[]
        this.currentPage = 1; // Current page number
        this.itemsPerPage = 20;
        this.rdpUserList=[]
       
        this.filterJobLocationField = this.page.add_field({
            label: 'Location',
            fieldtype: 'Data',
            fieldname: 'job_location',
            change: () => {
                this.filterData(
                    this.filterJobLocationField.get_value(),
                    this.filterFullName.get_value(),
                    this.statusFilter.get_value(),
                   
                    this.filterPlatform.get_value()
                );
            }
        });
        this.filterFullName = this.page.add_field({
            label: 'Full Name',
            fieldtype: 'Link',
            fieldname: 'user_id',
            options:"Excel RDP User",
            // get_query:function(){
            //     const rdp_user=[]
            //     frappe.call({
            //         method: 'frappe.client.get_list',
            //         args: {
            //             doctype: 'Excel RDP User',
            //             fields: ['email']
            //         },
            //         callback: (res) => {
            //             const data = res.message;
            //             console.log("data",data)
            //             if (Array.isArray(data)) {
            //                 data.forEach(x => {
            //                     if (x) {
            //                         console.log('x',x)
            //                         rdp_user.push(x.email);
            //                         console.log(rdp_user)
            //                     }
            //                 });
            //                 console.log('rdp_user',rdp_user)
            //                 return{
            //                     "filters":{
            //                         "name": ['in', rdp_user]
            //                     }
            //                 }
                            
                            
            //             }
                        
            //             // this.filterFullName.df.options = ["",...this.fullNamelist];
            //             // this.filterFullName.refresh()
            //             // console.log(this.fullNamelist); // Moved inside the callback to ensure it's logged after data is populated
            //         }
            //     });
            //     console.log('rdp_user',rdp_user)
                
               
            // },
            change: () => {
                console.log(this.filterFullName.get_value(),)
                this.filterData(
                    this.filterJobLocationField.get_value(),
                    this.filterFullName.get_value(),
                    this.statusFilter.get_value(),
                   
                    this.filterPlatform.get_value()
                );
            }
        });
        // device type

     
        // device type

        this.filterPlatform = this.page.add_field({
            label: 'Platform',
            fieldtype: 'Select',
            default: '',
            options: ['','Windows', 'Linux'],
            fieldname: 'user_id',
            change: () => {
                this.filterData(
                    this.filterJobLocationField.get_value(),
                    this.filterFullName.get_value(),
                    this.statusFilter.get_value(),
                   
                    this.filterPlatform.get_value()
                );
            }
        });
        // Set up the 'Refresh' button action
        this.refreshButton = this.page.set_secondary_action('Refresh', () => {
            this.getAndRenderRdpDeviceList().then(() => {
               
            }).catch((error) => {
                console.error("Error refreshing device list:", error);
                frappe.msgprint("Error refreshing device list. Please try again.");
            });
        });
        this.downloadInstaller = this.page.set_primary_action('Download Windows Installer', () => {
            window.location.href = 'https://github.com/excel-azmin/ArcRDP-Agent/raw/main/ArcCare.exe';
        });
        // Set up the 'Status' field
       this.statusFilter  = this.page.add_field({
            label: 'Status',
            fieldtype: 'Select',
            fieldname: 'status',
            default: '',
            options: ['','Online', 'Offline'],
            change: () => {
                this.filterData(
                    this.filterJobLocationField.get_value(),
                    this.filterFullName.get_value(),
                    this.statusFilter.get_value(),
                  
                    this.filterPlatform.get_value()
                );
            }
        });

        // Initial fetch and render of the device list
        this.getAndRenderRdpDeviceList();
        this.getFullName();
        
    }

    /**
     * Renders the device inventory template and sets up event listeners.
     */
    getFullName() {
        // frappe.call({
        //     method: 'frappe.client.get_list',
        //     args: {
        //         doctype: 'Excel RDP User',
        //         fields: ['email']
        //     },
        //     callback: (res) => {
        //         const data = res.message;
        //         this.rdpUserList=data
        //         if (Array.isArray(data)) {
                  
        //             console.log(data);
        //             this.fullNamelist = [];
        //             data.forEach(x => {
        //                 if (x) {
        //                     this.rdpUserList.push(x.full_name);
        //                 }
        //             });
                    
        //         }
        //         // this.filterFullName.df.options = ["",...this.fullNamelist];
        //         // this.filterFullName.refresh()
        //         // console.log(this.fullNamelist); // Moved inside the callback to ensure it's logged after data is populated
        //     }
        // });
        // This will log before the data is fetched due to asynchronous nature of frappe.call
    }
    
    renderTemplate() {
        $('#rdp_devices').remove();
        $('.pagination').remove();
        const startIndex = (this.currentPage - 1) * this.itemsPerPage;
        const endIndex = this.currentPage * this.itemsPerPage;
        const template = frappe.render_template('rdp_devices', { device_data: this.deviceData.slice(startIndex, endIndex) });
        $(template).appendTo(this.page.body);
    
        // Add pagination controls
        const totalPages = Math.ceil(this.deviceData.length / this.itemsPerPage);
        const paginationHTML = `
            <div class="pagination">
                <button type="button" class="btn btn-primary mr-2 btn-prev" ${this.currentPage === 1 ? 'disabled' : ''}>Prev</button>
                <span>Page ${this.currentPage} of ${totalPages}</span>
                <button type="button" class="btn btn-primary mr-2 btn-next" ${this.currentPage === totalPages ? 'disabled' : ''}>Next</button>
            </div>
        `;
        $(paginationHTML).appendTo(this.page.body);
    
        // Add event listeners for pagination buttons
        $('.btn-prev').on('click', () => {
            if (this.currentPage > 1) {
                this.currentPage--;
                this.renderTemplate();
            }
        });
    
        $('.btn-next').on('click', () => {
            if (this.currentPage < totalPages) {
                this.currentPage++;
                this.renderTemplate();
            }
        });
        $('.small-text.percent').each((index, element) => {
            // Extract the original value
            const originalValue = parseFloat($(element).text());
    
            // Ensure it's a number and not NaN
            if (!isNaN(originalValue)) {
                // Format the value to show only two digits after the decimal point
                const formattedValue =  (originalValue * 100)?.toFixed(2) + "%";
                $(element).text(formattedValue);
            }
        });
        $('.small-text.hour').each((index, element) => {
            // Extract the time string
            const timeString = $(element).text();
        
            // Splitting the time string into hours, minutes, and seconds
            const [hours, minutes, seconds] = timeString.split(':').map(parseFloat);
        
            // Calculating total minutes
            const totalMinutes = hours * 60 + minutes + seconds / 60;
        
            // Extracting the integer part for hours
            const totalHours = Math.floor(totalMinutes / 60);
        
            // Extracting the remaining minutes
            const remainingMinutes = Math.round(totalMinutes % 60);
        
            // Format the hours and minutes
            const formattedTime = `${totalHours}hr ${remainingMinutes}min`;
        
            // Update the content of the element with the formatted time
            $(element).text(formattedTime);
        });
        $('.small-text.datetime').each((index, element) => {
            // Extract the timestamp
            const timestamp = $(element).text();
            const date = new Date(timestamp);
            const formattedDate = `${date.getDate().toString().padStart(2, '0')}/${(date.getMonth() + 1).toString().padStart(2, '0')}/${date.getFullYear()}`;
            const formattedTime = `${date.getHours().toString().padStart(2, '0')}:${date.getMinutes().toString().padStart(2, '0')}`;
            // Format the timestamp
        
            // Update the content of the element with the formatted date and time
            $(element).text(`${formattedDate} ${formattedTime}`);
        });
        
        // $('.percent').each((index, element) => {
        //     const sizeGB = parseFloat($(element).text());
        //     const percent = (sizeGB / MAX_SIZE_GB) * 100; // Assuming MAX_SIZE_GB is the maximum size in GB
        //     $(element).text(percent.toFixed(2) + '%');
        // });
        // Add event listener for 'Connect' buttons
        $('.connect').on('click', (event) => {
            const rowIndex = $(event.target).closest('tr').index();
            const device = this.deviceData[rowIndex];
            frappe.call({
                method: 'excel_helpdesk.excel_helpdesk.page.rdp_devices.rdp_devices.get_rdp_url',
                args: { id: device.id },
                callback: function (response) {
                    window.open(response.message);
                }
            });
        });
        $('.delete').on('click', (event) => {
            const rowIndex = $(event.target).closest('tr').index();
            const device = this.deviceData[rowIndex];
          
            frappe.call({
                method: 'excel_helpdesk.excel_helpdesk.page.rdp_devices.rdp_devices.delete_agent_from_device',
                args: { id: device.id },
                callback: function (response) {
                    console.log(response)
                   frappe.msgprint('device uninstall queue started')
                }
            });
        });
        $('.drive').on('click', (event) => {
            const rowIndex = $(event.target).closest('tr').index();
            const device = this.deviceData[rowIndex];

            // Generate HTML for the list of drive details
            let modalContent = `
            <div class="modal-body">
            <div>
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th>Drive Format</th>
                            <th>Drive Type</th>
                            <th>Free Space (GB)</th>
                            <th>Name</th>
                            <th>Root Directory</th>
                            <th>Total Size (GB)</th>
                            <th>Volume Label</th>
                        </tr>
                    </thead>
                    <tbody>
                        ${device.drives.map(drive => `
                            <tr>
                                <td>${drive.driveFormat}</td>
                                <td>${drive.driveType}</td>
                                <td>${drive.freeSpace}</td>
                                <td>${drive.name}</td>
                                <td>${drive.rootDirectory}</td>
                                <td>${drive.totalSize}</td>
                                <td>${drive.volumeLabel}</td>
                            </tr>
                        `).join('')}
                    </tbody>
                </table>
            </div>
        </div>
        
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            `;

            // Set modal content
            $('#driveModal .modal-content').html(modalContent);

            // Show the modal
            $('#driveModal').modal('show');
        });

    }

    /**
     * Fetches the list of RDP devices and renders them using the template.
     * 
     */
    filterData(jobLocation = null, fullName = null,status=null,Platform=null){
        console.log(this.rdpData)
        let filteredData = this.rdpData.filter(device => {
            // Check if the device matches the selected job location
            const matchesJobLocation = jobLocation ? new RegExp(jobLocation, 'i').test(device.location) : true;
            // Check if the device matches the selected user ID
            const mathFullName = fullName ? device.email === fullName : true;
           
            const matchPlatform=Platform ? device.platform === Platform : true;
            if (status === 'Online') {
                return matchesJobLocation && mathFullName && device.isOnline && matchPlatform
            } else if (status === 'Offline') {
                return matchesJobLocation && mathFullName && !device.isOnline &&  matchPlatform
            } else {
                return matchesJobLocation && mathFullName && matchPlatform
            }
        });
        console.log("filter_data",filteredData)
        this.deviceData = filteredData; 
        console.log(this.deviceData);
        // Assign mergeData instead of data
       
        this.renderTemplate();
    }
    async getAndRenderRdpDeviceList(jobLocation = null, fullName = null,status=null,Platform=null) {
        try {
            
            const data = await frappe.xcall('excel_helpdesk.excel_helpdesk.page.rdp_devices.rdp_devices.get_rdp_devices_list');
            if (Array.isArray(data)) {
                const mergeDataPromises = data.map(async (device) => {
                    try {
                        const response = await frappe.db.get_value('Excel RDP User', { 'device_id': device.id }, ['*']);
                        const user_device_data = response.message;
                        return { ...device, ...user_device_data };
                    } catch (error) {
                        frappe.msgprint("Error retrieving user data for device:", JSON.stringify(device));
                        return { ...device };
                    }
                });

                const mergeData = await Promise.all(mergeDataPromises);
                console.log(mergeData)
                // rdp data 
              this.rdpData=[...mergeData]
              this.filterData();
               
            } else {
                console.error("Invalid data format received from get_rdp_devices_list");
                frappe.msgprint("Received invalid device data. Please contact the administrator.");
            }
        } catch (error) {
            console.error('Error retrieving RDP devices:', error);
            frappe.msgprint("Error retrieving device list. Please try again.");
        }
    }

}

// Initialize the device inventory page when the corresponding Frappe page is loaded
frappe.pages['rdp-devices'].on_page_load = (wrapper) => {
    new DeviceInventory(wrapper);
};
