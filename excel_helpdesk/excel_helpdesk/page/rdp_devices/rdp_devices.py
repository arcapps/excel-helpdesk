import requests
import base64
import frappe

@frappe.whitelist()
def get_rdp_devices_list():
    settings = frappe.get_doc("Excel Helpdesk Settings")
    url = f"{settings.rdp_server_url}/api/Devices"
    
    # Ensure the correct use of API tokens/credentials
    credentials = f"{settings.rdp_auth_key}:{settings.rdp_auth_secret}"
    base64_credentials = base64.b64encode(credentials.encode('utf-8')).decode('utf-8')
    
    headers = {
        'Authorization': f"Basic {base64_credentials}"
    }
    
    try:
        response = requests.get(url, headers=headers)
        response.raise_for_status()  # Raise an error for bad HTTP status codes
        return response.json()  # Assuming the response is in JSON format
    except requests.exceptions.RequestException as e:
        frappe.log_error(f"Error in RDP API request: {str(e)}")
        return {"error": str(e)}  # Return an error message in case of failure

@frappe.whitelist()
def get_rdp_url(id):
    try:
      
        settings = frappe.get_doc("Excel Helpdesk Settings")
        url = f"{settings.rdp_server_url}/api/RemoteControl/{id}"
        
        credentials = f"{settings.rdp_auth_key}:{settings.rdp_auth_secret}"
        base64_credentials = base64.b64encode(credentials.encode('utf-8')).decode('utf-8')
        headers = {'Authorization': f"Basic {base64_credentials}"}
       
        response = requests.get(url, headers=headers, timeout=300)
        response.raise_for_status() 
        
        # Raises an HTTPError for bad responses
        return response.text  # Directly return the response text
    except requests.exceptions.RequestException as e:
        frappe.log_error(str(e))
        frappe.msgprint("Error fetching RDP URL. Please try again later.")
@frappe.whitelist()
def delete_agent_from_device (id):
    try:
      
        settings = frappe.get_doc("Excel Helpdesk Settings")
        url = f"{settings.rdp_server_url}/api/UninstallDevice/{id}"
        
        credentials = f"{settings.rdp_auth_key}:{settings.rdp_auth_secret}"
        base64_credentials = base64.b64encode(credentials.encode('utf-8')).decode('utf-8')
        headers = {'Authorization': f"Basic {base64_credentials}"}
       
        response = requests.get(url, headers=headers, timeout=300)
        # Raises an HTTPError for bad responses
        return True  # Directly return the response text
    except requests.exceptions.RequestException as e:
        return True
@frappe.whitelist()
def enqueue_get_rdp_url(id):
    # Enqueue the long-running task
    job = frappe.enqueue('excel_helpdesk.excel_helpdesk.page.rdp_devices.rdp_devices.get_rdp_url', id=id, timeout=300)
    frappe.msgprint(f"Fetching RDP URL in the background. Job ID: {job.get('job_name')}")
