import frappe
from frappe import auth
@frappe.whitelist( allow_guest=True )
def test():
    return 'work'
@frappe.whitelist( allow_guest=True )
def rdp_login(usr, pwd):
    try:
        login_manager = frappe.auth.LoginManager()
        login_manager.authenticate(user=usr, pwd=pwd)
        login_manager.post_login()
    except frappe.exceptions.AuthenticationError:
        frappe.clear_messages()
        frappe.local.response['http_status_code'] = 401

        frappe.local.response["message"] = {
            "success_key":0,
            "message":"Authentication Error!"
        }

        return

   
    user = frappe.get_doc('User', frappe.session.user)
    help_desk = frappe.get_doc('Excel Helpdesk Settings')
    frappe.local.response['http_status_code'] = 200

    frappe.response["message"] = {
        "success_key":1,
        "message":"Authentication success",
        "rdp_server_url":help_desk.rdp_server_url,
        "rdp_organization_id":help_desk.rdp_organization_id,
        "erp_auth_key":help_desk.erp_auth_key,
        "erp_auth_secret":help_desk.erp_auth_secret,
        "username":user.username,
        "email":user.email,
        "location":user.location,
    }



@frappe.whitelist()
def get_team_member(parent):
    dynamic_parent = parent
    result = frappe.db.sql(
            """
            SELECT m.user
            FROM `tabSupport Team Member` AS m
            WHERE m.parenttype='ArcApps Support Team' AND m.parent=%s
            """,
            (dynamic_parent,),
            as_dict=True
        )
    result=extract_emails(result)
    return result
def extract_emails(users_list):
    return [user['user'] for user in users_list]


@frappe.whitelist( allow_guest=True)
def test_id(userid):
    return userid



@frappe.whitelist()
def get_user_by_device_id(device_id=None):
    is_exists_device_id=frappe.db.get_value('Excel RDP User',{"device_id":device_id},'email')
    if is_exists_device_id is None:
        frappe.local.response['http_status_code'] = 404
        frappe.local.response['error'] = "device id not exists"
        return
    user= frappe.get_doc("User",is_exists_device_id)
    frappe.local.response['http_status_code'] = 200
    frappe.response["message"] = {
        "Name":f"{user.first_name} {user.middle_name}",
        "SureName":user.last_name,
        "Email":user.name,
        "Cellphone":user.mobile_no
    }